package com.company;

import java.util.Scanner;

public class Main {
    public static int divide(int num1, int num2) {
        return num1 / num2;
    }

    public static void main(String[] args) {
        // prompt the user to enter input
        Scanner input = new Scanner(System.in);
        System.out.print("Input the first number (numerator): ");
        int num1 = input.nextInt();
        System.out.print("Input the second number (denominator): ");
        int num2 = input.nextInt();
        System.out.println();

        try {
            int result = num1 / num2;
            System.out.println("Division result of " + num1 + "/" + num2 + "= " + result);
        } catch (ArithmeticException e) {
            System.out.println("** Exception is caught: ");
            System.out.println("please do not divide a number by zero");
        }
    }
}




