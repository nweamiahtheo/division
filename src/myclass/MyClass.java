package myclass;
import java.util.Scanner;

public class MyClass {

    public static void main(String[] args) {
        // public int division (num1, num2)
        // get user input from scanner
        Scanner scanner = new Scanner(System.in);


        while (true) {
            try {
                // get user input
                System.out.println("enter num1 numerator");
                int num1 = scanner.nextInt();
                //get user input
                System.out.println("enter num2");
                int num2 = scanner.nextInt();
                int result = num1/num2;
                // show output
                System.out.println("Division result of " + num1 + "/" + num2 + "= " + result);

                break;
                // catch exception
            } catch (ArithmeticException e) {
                System.out.println("** Zero is not allowed: ");
                System.out.println("Please try again");
            }
        }
    }
}

